import React, { useContext } from 'react';
import AppContext from './AppContext';

import { ThemeProvider } from '@material-ui/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';

import LanguageIcon from '@material-ui/icons/Language';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const {
  LangContext,
  LangProvider,
  ModeContext,
  ModeProvider,
  StyleContext,
  StyleProvider
} = AppContext;

function Main() {
  return (
    <StyleProvider>
    <LangProvider>
    <ModeProvider>
      <Navbar />
      <Content />
      <Footer />
    </ModeProvider>
    </LangProvider>
    </StyleProvider>
  );
}

function Navbar(){
  const langProps = useContext(LangContext);
  const modeProps = useContext(ModeContext);
  const styleProps = useContext(StyleContext);
  return(
    <AppBar position="fixed">
      <Container maxWidth="md" disableGutters>
        <Toolbar>
          <Typography variant="h6" className={styleProps.classes.title}>Article</Typography>
          
          {/* Language */}
          <Button
            color="inherit"
            startIcon={<LanguageIcon />}
            endIcon={<ArrowDropDownIcon />}
            onClick={langProps.openLangMenu}>
            {langProps.lang}
          </Button>
          <Menu
            id="lang-menu"
            anchorEl={langProps.anchorElLang}
            keepMounted
            open={Boolean(langProps.anchorElLang)}
            onClose={langProps.closeLangMenu}>
            <MenuItem onClick={(e) => langProps.handleMenuItemClick(e,'english')}>English</MenuItem>
            <MenuItem onClick={(e) => langProps.handleMenuItemClick(e,'bahasa')}>Bahasa</MenuItem>
          </Menu>

          {/* Mode */}
          <Button
            color="inherit"
            startIcon={<Brightness4Icon />}
            endIcon={<ArrowDropDownIcon />}
            onClick={modeProps.openModeMenu}>
            {modeProps.mode}
          </Button>
          <Menu
            id="lang-menu"
            anchorEl={modeProps.anchorElMode}
            keepMounted
            open={Boolean(modeProps.anchorElMode)}
            onClose={modeProps.closeModeMenu}>
            <MenuItem onClick={(e) => modeProps.handleMenuItemClick(e,'light')}>Light</MenuItem>
            <MenuItem onClick={(e) => modeProps.handleMenuItemClick(e,'dark')}>Dark</MenuItem>
          </Menu>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

function Content(){
  const langProps = useContext(LangContext);
  const modeProps = useContext(ModeContext);
  const styleProps = useContext(StyleContext);
  return(
    <ThemeProvider theme={modeProps.modeStyle}>
    <Paper>
      <Container maxWidth="sm" className={styleProps.classes.content}>
        <Typography variant="body1" className={styleProps.classes.paragraph}>
          {langProps.p[0]}
        </Typography>
        <Typography variant="body1" className={styleProps.classes.paragraph}>
          {langProps.p[1]}
        </Typography>
        <Typography variant="body1" className={styleProps.classes.paragraph}>
          {langProps.p[2]}  
        </Typography>
      </Container>
    </Paper>
    </ThemeProvider>
  );
}

function Footer(){
  const styleProps = useContext(StyleContext);
  return (
    <div className={styleProps.classes.footer}>
      <Typography variant="overline" display="block">Made by Ahmad Khoirunnufus</Typography>
    </div>
  );
}

export default Main;