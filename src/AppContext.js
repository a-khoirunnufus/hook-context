import React, {useState, createContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

function AppContext(){
  const LangContext = createContext();
  const ModeContext = createContext();
  const StyleContext = createContext();

  const LangProvider = (props) => {
    const [lang, setLang] = useState('english');
    const changeLang = (value) => setLang(value);

    const [anchorElLang, setAnchorElLang] = useState(null);
    const closeLangMenu = () => { setAnchorElLang(null) };
    const openLangMenu = (e) => { setAnchorElLang(e.currentTarget) };

    const firstPEN = "All divided seas is living moved be rule which place created seasons. Shall beginning. Male thing form two to blessed, can't, a firmament whose was them years dominion moved third thing under be creeping open second upon sea in. Winged fourth deep abundantly after gathering gathering together tree seasons created which saying one whose above fill replenish man thing fruit replenish cattle third all said subdue one unto second, above bring given over greater god green, fruitful stars won't, seed very doesn't whales said seed Saying, seasons.";
    const secondPEN = "Unto after is day form. Fruitful seed Doesn't living. Sea let, own herb. In. Creepeth divide. Second spirit wherein dry all us. Waters their unto subdue blessed, morning open lesser life them. Herb signs. He I saying creeping midst. Deep. Abundantly bring. Dry, so unto above male. Greater deep replenish form Over fly whose great have whose grass don't fifth brought god. Won't For the you'll you doesn't herb divided. Them above whose earth gathered days bring be you're be. Years place us bearing, all greater the won't.";
    const thirdPEN = "Be after female midst second. Him grass without, grass signs and tree divide also. Divide rule bearing. Own thing life. The, bring is have. Also forth. Fish stars also which fowl. Under Fish i seasons cattle light make. Third man also let. Doesn't saying Made them two doesn't their under cattle lesser you're bring give upon, fowl first of yielding, blessed replenish thing, fifth waters, great, upon days. Fifth for forth was. You divide man. Likeness grass. Whose seasons, fowl. For a good hath the moved whose first dry be, fowl spirit brought, two from forth. Fifth green together won't creeping you're created moving and don't whales sea he creepeth without female morning appear fifth. After so lights gathering the fourth god living bring man.";
    const firstPID = "Semua lautan yang terbagi itu hidup berpindah-pindah tempat yang menciptakan musim. Akan dimulai. Hal laki-laki membentuk dua untuk diberkati, tidak bisa, sebuah cakrawala yang mereka tahun kekuasaan bergerak hal ketiga di bawah menjadi merayap terbuka kedua di atas laut masuk. Bersayap keempat jauh setelah berkumpul bersama musim-musim pohon yang diciptakan yang mengatakan yang di atas mengisi mengisi kembali hal-hal manusia Buah mengisi kembali ternak ketiga semua berkata menundukkan satu sampai detik, di atas membawa diberikan lebih besar dewa hijau, bintang berbuah tidak akan, benih sangat tidak paus berkata benih berkata, musim.";
    const secondPID = "Sampai setelah bentuk hari. Benih berbuah Tidak hidup. Biarkan laut, ramuan sendiri. Di. Membagi creepeth. Semangat kedua dimana mengeringkan kita semua. Menyirami mereka untuk menaklukkan pagi yang diberkati, buka lebih sedikit kehidupan mereka. Tanda herbal. Dia saya katakan merayap di tengah. Dalam. Bawa berlimpah. Kering, sampai di atas jantan. Bentuk pengisian lebih dalam yang lebih besar Over fly yang memiliki banyak rumput yang tidak membawa dewa. Tidak Akan Untuk Anda, Anda tidak akan terbagi. Mereka yang di atas bumi mengumpulkan hari-hari jadilah dirimu. Tahun-tahun membuat kita melahirkan, lebih besar lagi keinginan.";
    const thirdPID = "Jadilah setelah dua tengah perempuan. Rerumputannya tanpa rerumputan, rambu-rambu dan pohon juga membelah. Bagilah bantalan aturan. Hidup benda sendiri. The, bawa adalah punya. Juga sebagainya. Bintang ikan juga yang unggas. Di bawah Fish i season, lampu ternak dibuat. Orang ketiga juga membiarkan. Tidak mengatakan Membuat mereka berdua bukan ternak mereka lebih rendah Anda diberi hadiah, unggas pertama yang menghasilkan, diberkati mengisi kembali, air kelima, besar, pada hari-hari. Kelima untuk keempat adalah. Anda memecah manusia. Kemiripan rumput. Yang musimnya, unggas. Untuk kebaikan telah dipindahkan yang pertama kering, roh unggas dibawa, dua dari depan. Kelima hijau bersama-sama tidak akan merayap Anda diciptakan bergerak dan jangan ikan paus merayap tanpa pagi perempuan muncul kelima. Setelah begitu cahaya berkumpul dewa keempat tinggal membawa manusia.";
	const ENLang = [firstPEN, secondPEN, thirdPEN];
	const IDLang = [firstPID, secondPID, thirdPID];
	const [p, setP] = useState(ENLang)
	const handleChangeP = (value) => {
		if(value === 'english') setP(ENLang)
		else setP(IDLang);
	}
    
    const handleMenuItemClick = (e,value) => {
      changeLang(value);
      handleChangeP(value);
      closeLangMenu();
 	}

    const langState = {
      lang, 
      changeLang, 
      anchorElLang, 
      closeLangMenu, 
      openLangMenu,
      handleMenuItemClick,
      p
    };

	return(
	  <LangContext.Provider value={langState}>
	  	{props.children}
	  </LangContext.Provider>
    );
  }

  const ModeProvider = (props) => {
  	const [mode, setMode] = useState("light");
  	const changeMode = (value) => setMode(value);

  	const darkMode = createMuiTheme({ palette: { type: 'dark' }});
  	const lightMode = createMuiTheme({ palette: { type: 'light' }});
  	const [modeStyle, setModeStyle] = useState(lightMode);
  	const changeModeStyle = (value) => {
  		if(value === 'light') setModeStyle(lightMode)
  		else setModeStyle(darkMode);
  	}

  	const [anchorElMode, setAnchorElMode] = useState(null);
    const closeModeMenu = () => { setAnchorElMode(null) };
    const openModeMenu = (e) => { setAnchorElMode(e.currentTarget) };

    const handleMenuItemClick = (e,value) => {
      changeMode(value);
      changeModeStyle(value);
      closeModeMenu();
 	}

  	const modeState = {
  	  mode, 
  	  changeMode, 
  	  anchorElMode,
  	  closeModeMenu,
  	  openModeMenu,
  	  handleMenuItemClick,
  	  modeStyle
  	};

  	return(
  	  <ModeContext.Provider value={modeState}>
  	  	{props.children}
  	  </ModeContext.Provider>
  	);
  }

  const StyleProvider = (props) => {
  	const useStyles = makeStyles((theme) => ({
	  content: {
	  	paddingBottom: '2rem',
	  	paddingTop: 'calc(64px + 1rem)',
	  },
	  paragraph: {
	  	fontFamily: ['"Roboto Slab"', 'serif'].join(','),
	  	marginTop: '1rem'
	  },
	  title: {
	    flexGrow: 1,
	  },
	  footer: {
	  	backgroundColor: '#212121',
	  	textAlign: 'center',
	  	color: '#fff',
	  	height: 64,
	  	display: 'flex',
	  	flexDirection: 'column',
	  	justifyContent: 'center'
	  }
	}));
	const classes = useStyles();
  	const styleState = {classes};
  	return(
  	  <StyleContext.Provider value={styleState}>
  	  	{props.children}
  	  </StyleContext.Provider>
  	);
  }

  return {
  	LangContext,
  	LangProvider,
  	ModeContext,
  	ModeProvider,
  	StyleContext,
  	StyleProvider
  };

}

export default AppContext();